"use strict";

const formatObject = require('./src/formater');
const { mapObject } = require('./src/utils');
const enricher = require('./src/enricher');
const validateData = require('./src/validator');
const getQueries = require('./src/getQueries');
const generateError = require('./src/errorsHandler');

module.exports = {
  formatObject,
  mapObject,
  validateData,
  enricher,
  getQueries,
  generateError,
};
