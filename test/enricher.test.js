const {
  enrichInvitations,
  enrichInvitation,
  enrichMeetings,
  enrichMeeting,
  enrichUsers,
  enrichUser,
  enrichRooms,
  enrichRoom,
  enrichBeacons,
  enrichBeacon,
  enrichTeamInvitations,
  enrichTeamInvitation,
} = require('../src/enricher');

const { mockedData, enrichedMockedData } = require('./mocks/enrichedData');

const meetings = [mockedData.meeting, mockedData.meeting];
const users = [mockedData.user, mockedData.user];
const rooms = [mockedData.room, mockedData.room];
const beacons = [mockedData.beacon, mockedData.beacon];
const teamInvitations = [mockedData.teamInvitation, mockedData.teamInvitation];

describe('invitations', () => {
  test('return null if some parameters are missing', () => {
    expect(enrichInvitation()).toBe(null);
  });

  test('return enriched invitation when provided correct data', () => {
    const enrichedInvitation = enrichInvitation(mockedData.invitation, users, rooms, beacons);

    expect(enrichedInvitation).toEqual(enrichedMockedData.invitation);
  });

  test('return enriched meetings when provided correct data', () => {
    const enrichedInvitations = enrichMeetings(meetings, users, rooms, beacons);

    const expectedEnrichedInvitations = [enrichedMockedData.invitation, enrichedMockedData.invitation];

    expect(enrichedInvitations).toEqual(expectedEnrichedInvitations);
  });
});

describe('meetings', () => {
  test('return null if some parameters are missing', () => {
    expect(enrichMeeting()).toBe(null);
  });

  test('return enriched meeting when provided correct data', () => {
    const enrichedMeeting = enrichMeeting(mockedData.meeting, users, rooms, beacons);

    expect(enrichedMeeting).toEqual(enrichedMockedData.meeting);
  });

  test('return enriched meetings when provided correct data', () => {
    const enrichedMeetings = enrichMeetings(meetings, users, rooms, beacons);

    const expectedEnrichedMeetings = [enrichedMockedData.meeting, enrichedMockedData.meeting];

    expect(enrichedMeetings).toEqual(expectedEnrichedMeetings);
  });
});

describe('users', () => {
  test('return null if some parameters are missing', () => {
    expect(enrichUser()).toBe(null);
  });

  test('return enriched user when provided correct data', () => {
    const enrichedUser = enrichUser(mockedData.user, rooms, beacons);

    expect(enrichedUser).toEqual(enrichedMockedData.user);
  });

  test('return enriched users when provided correct data', () => {
    const enrichedUsers = enrichUsers(users, rooms, beacons);

    const expectedEnrichedUsers = [enrichedMockedData.user, enrichedMockedData.user];

    expect(enrichedUsers).toEqual(expectedEnrichedUsers);
  });
});

describe('rooms', () => {
  test('return null if some parameters are missing', () => {
    expect(enrichRoom()).toBe(null);
  });

  test('return enriched room when provided correct data', () => {
    const enrichedRoom = enrichRoom(mockedData.room, beacons);

    expect(enrichedRoom).toEqual(enrichedMockedData.room);
  });

  test('return enriched rooms when provided correct data', () => {
    const enrichedRooms = enrichRooms(rooms, beacons);

    const expectedEnrichedRooms = [enrichedMockedData.room, enrichedMockedData.room];

    expect(enrichedRooms).toEqual(expectedEnrichedRooms);
  });
});

describe('beacons', () => {
  test('return null if some parameters are missing', () => {
    expect(enrichBeacon()).toBe(null);
  });

  test('return enriched beacon when provided correct data', () => {
    const enrichedBeacon = enrichBeacon(mockedData.beacon);

    expect(enrichedBeacon).toEqual(enrichedMockedData.beacon);
  });

  test('return enriched beacons when provided correct data', () => {
    const enrichedBeacons = enrichBeacons(beacons);

    const expectedEnrichedBeacons = [enrichedMockedData.beacon, enrichedMockedData.beacon];

    expect(enrichedBeacons).toEqual(expectedEnrichedBeacons);
  });
});

describe('teamInvitations', () => {
  test('return null if some parameters are missing', () => {
    expect(enrichTeamInvitation()).toBe(null);
  });

  test('return enriched team invitation when provided correct data', () => {
    const enrichedTeamInvitation = enrichTeamInvitation(mockedData.teamInvitation);

    expect(enrichedTeamInvitation).toEqual(enrichedMockedData.teamInvitation);
  });

  test('return enriched team invitations when provided correct data', () => {
    const enrichedTeamInvitations = enrichTeamInvitations(teamInvitations);

    const expectedEnrichedTeamInvitations = [enrichedMockedData.teamInvitation, enrichedMockedData.teamInvitation];

    expect(enrichedTeamInvitations).toEqual(expectedEnrichedTeamInvitations);
  });
});
