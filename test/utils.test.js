const { mapObject } = require('../src/utils');

test('return empty array if no valid or empty object is provided', () => {
  const result1 = mapObject(null, (value, key) => value);
  const result2 = mapObject(undefined, (value, key) => value);
  const result3 = mapObject('', (value, key) => value);
  const result4 = mapObject({}, (value, key) => value);
  const result5 = mapObject(5, (value, key) => value);

  expect(result1).toEqual([]);
  expect(result2).toEqual([]);
  expect(result3).toEqual([]);
  expect(result4).toEqual([]);
  expect(result5).toEqual([]);
});

test('map through object properties and uses callback on each property', () => {
  const objectToMap = {
    2: {},
    property1: 1,
    property2: 'string',
    '48dwdwqDW456': [''],
  }

  const keys = mapObject(objectToMap, (value, key) => key);
  const values = mapObject(objectToMap, (value, key) => value);

  expect(keys).toEqual(['2', 'property1', 'property2','48dwdwqDW456']);
  expect(values).toEqual([{}, 1, 'string', ['']]);
});
