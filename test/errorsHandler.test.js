const generateError = require('../src/errorsHandler');

test('return an error object when provided error ID and additional param', () => {
  const error = generateError(1000, {error: 'error'});

  expect(error).toEqual({
    code: 1000,
    message: 'No valid validation schema provided',
    errors: {
      error: 'error',
    }
  });
});
