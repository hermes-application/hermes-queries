const formatObject = require('../src/formater');

const mockedParseObject = {
  get: function(param) {
    return this[param];
  },
}

test('return null if incorrect data is provided', () => {
  expect(formatObject()).toBe(null);
});

test('throw error if incorrect type is provided', () => {
  expect(() => formatObject({}, 'INCORRECT_TYPE')).toThrow();
});

describe('team', () => {
  test('return formatted object when provided correct data', () => {
    const team = Object.assign({}, mockedParseObject, {
      id: 1,
      customId: 1,
      name: 'team',
      billingId: 1,
      addressStreet: 'street',
      addressSuburb: 'suburb',
      addressState: 'state',
      email: 'team@email.com',
      status: 'active',
    });

    const formattedTeam = formatObject(team, 'teams');

    expect(formattedTeam).toEqual({
      id: 1,
      type: 'teams',
      name: 'team',
      billingId: 1,
      addressStreet: 'street',
      addressSuburb: 'suburb',
      addressState: 'state',
      email: 'team@email.com',
      customId: 1,
      status: 'active',
    });
  });

  test('return formatted object when provided correct data with missing fields', () => {
    const team = Object.assign({}, mockedParseObject, {
      id: 1,
      customId: 1,
      name: 'team',
      email: 'team@email.com',
      status: 'active',
    });

    const formattedTeam = formatObject(team, 'teams');

    expect(formattedTeam).toEqual({
      id: 1,
      type: 'teams',
      name: 'team',
      billingId: null,
      addressStreet: null,
      addressSuburb: null,
      addressState: null,
      email: 'team@email.com',
      customId: 1,
      status: 'active',
    });
  });
});

describe('meeting/invitation', () => {
  test('return formatted object when provided correct data', () => {
    const meeting = Object.assign({}, mockedParseObject, {
      id: 1,
      name: 'meeting',
      startTime: new Date('October 13, 2014 11:13:00'),
      endTime: new Date('October 13, 2014 11:13:00'),
      organizerId: 1,
      roomId: 1,
      guestsPendingIds: [1, 1],
      guestsAcceptedIds: [1, 1],
      guestsRefusedIds: [1, 1],
      status: 'active',
    });

    const formattedMeeting = formatObject(meeting, 'meetings');
    const formattedInvitation = formatObject(meeting, 'invitations');

    const result = {
      id: 1,
      type: 'meetings',
      name: 'meeting',
      startTime: new Date('October 13, 2014 11:13:00').toISOString(),
      endTime: new Date('October 13, 2014 11:13:00').toISOString(),
      organizerId: 1,
      roomId: 1,
      guestsPendingIds: [1, 1],
      guestsAcceptedIds: [1, 1],
      guestsRefusedIds: [1, 1],
      status: 'active',
    };

    expect(formattedMeeting).toEqual(result);

    result.type = 'invitations';

    expect(formattedInvitation).toEqual(result);
  });

  test('return formatted object when provided correct data with missing fields', () => {
    const meeting = Object.assign({}, mockedParseObject, {
      id: 1,
      startTime: new Date('October 13, 2014 11:13:00'),
      endTime: new Date('October 13, 2014 11:13:00'),
      roomId: 1,
      status: 'active',
    });

    const formattedMeeting = formatObject(meeting, 'meetings');
    const formattedInvitation = formatObject(meeting, 'invitations');

    const result = {
      id: 1,
      type: 'meetings',
      name: null,
      startTime: new Date('October 13, 2014 11:13:00').toISOString(),
      endTime: new Date('October 13, 2014 11:13:00').toISOString(),
      organizerId: null,
      roomId: 1,
      guestsPendingIds: [],
      guestsAcceptedIds: [],
      guestsRefusedIds: [],
      status: 'active',
    };

    expect(formattedMeeting).toEqual(result);

    result.type = 'invitations';

    expect(formattedInvitation).toEqual(result);
  });
});

describe('user', () => {
  test('return formatted object when provided correct data', () => {
    const user = Object.assign({}, mockedParseObject, {
      id: 1,
      searchUsername: 'user',
      userEmail: 'user@email.com',
      firstName: 'John',
      lastName: 'Doe',
      jobPosition: 'Unit Tester',
      phoneNumber: '0400000000',
      avatar: {
        url: () => 'https://avatar.com/avatar.jpg'
      },
      currentRoomId: 1,
      createdAt: new Date('October 13, 2014 11:13:00'),
      teamId: 1,
      role: 'admin',
      status: 'active',
    });

    const formattedUser = formatObject(user, 'users');

    expect(formattedUser).toEqual({
      id: 1,
      type: 'users',
      username: 'user',
      email: 'user@email.com',
      firstName: 'John',
      lastName: 'Doe',
      jobPosition: 'Unit Tester',
      phoneNumber: '0400000000',
      avatarUrl: 'https://avatar.com/avatar.jpg',
      currentRoomId: 1,
      joinDate: new Date('October 13, 2014 11:13:00').toISOString(),
      teamId: 1,
      role: 'admin',
      status: 'active',
    });
  });

  test('return formatted object when provided correct data with missing fields', () => {
    const user = Object.assign({}, mockedParseObject, {
      id: 1,
      searchUsername: 'user',
      userEmail: 'user@email.com',
      firstName: 'John',
      lastName: 'Doe',
      createdAt: new Date('October 13, 2014 11:13:00'),
      role: 'admin',
      status: 'active',
    });

    const formattedUser = formatObject(user, 'users');

    expect(formattedUser).toEqual({
      id: 1,
      type: 'users',
      username: 'user',
      email: 'user@email.com',
      firstName: 'John',
      lastName: 'Doe',
      jobPosition: null,
      phoneNumber: null,
      avatarUrl: null,
      currentRoomId: null,
      joinDate: new Date('October 13, 2014 11:13:00').toISOString(),
      role: 'admin',
      status: 'active',
    });
  });
});

describe('room', () => {
  test('return formatted object when provided correct data', () => {
    const room = Object.assign({}, mockedParseObject, {
      id: 1,
      name: 'room',
      notes: '4th floor',
      color: '#ffffff',
      beaconsIds: [1, 1],
      status: 'active',
    });

    const formattedRoom = formatObject(room, 'rooms');

    expect(formattedRoom).toEqual({
      id: 1,
      type: 'rooms',
      name: 'room',
      notes: '4th floor',
      color: '#ffffff',
      beaconsIds: [1, 1],
      status: 'active',
    });
  });

  test('return formatted object when provided correct data with missing fields', () => {
    const room = Object.assign({}, mockedParseObject, {
      id: 1,
      name: 'room',
      color: '#ffffff',
      status: 'active',
    });

    const formattedRoom = formatObject(room, 'rooms');

    expect(formattedRoom).toEqual({
      id: 1,
      type: 'rooms',
      name: 'room',
      notes: null,
      color: '#ffffff',
      beaconsIds: [],
      status: 'active',
    });
  });
});

describe('beacon', () => {
  test('return formatted object when provided correct data', () => {
    const beacon = Object.assign({}, mockedParseObject, {
      id: 1,
      uuid: '5672-3213-2132-0978',
      range: 7,
      status: 'active',
    });

    const formattedBeacon = formatObject(beacon, 'beacons');

    expect(formattedBeacon).toEqual({
      id: 1,
      type: 'beacons',
      uuid: '5672-3213-2132-0978',
      range: 7,
      status: 'active',
    });
  });
});

describe('teamInvitation', () => {
  test('return formatted object when provided correct data', () => {
    const teamInvitation = Object.assign({}, mockedParseObject, {
      id: 1,
      email: 'user@email.com',
      firstName: 'John',
      lastName: 'Doe',
      updatedAt: new Date('October 13, 2014 11:13:00'),
      status: 'active',
    });

    const formattedTeamInvitation = formatObject(teamInvitation, 'teamInvitations');

    expect(formattedTeamInvitation).toEqual({
      id: 1,
      type: 'teamInvitations',
      email: 'user@email.com',
      firstName: 'John',
      lastName: 'Doe',
      sentDate: new Date('October 13, 2014 11:13:00').toISOString(),
      status: 'active',
    });
  });
});
