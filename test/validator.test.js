const validateData = require('../src/validator');

test('return an error 1000 if no valid validation schema is provided', () => {
  const data = {};
  const result = validateData(data, undefined);

  expect(result.code).toBe(1000);
});

test('return an error if a parameter has not been trimmed and is not "passsword"', () => {
  const data = {
    value: ' nottrimmed ',
  };

  const validationSchema = {
    value: [],
  };

  const result = validateData(data, validationSchema);

  expect(result.code).toBe(1001);
  expect(result.errors.value).toBe('trim');
});

test('return success if a parameter has not been trimmed but is "passsword"', () => {
  const data = {
    value: ' nottrimmed ',
  };

  const validationSchema = {
    value: ['password'],
  };

  const result = validateData(data, validationSchema);

  expect(result).toBe(true);
});

test('return an error if a required parameter is missing', () => {
  const data = {};

  const validationSchema = {
    value: ['required'],
  };

  const result = validateData(data, validationSchema);

  expect(result.code).toBe(1001);
  expect(result.errors.value).toBe('required');
});

test('return success if input is not provided but also not required', () => {
  const data = {};

  const validationSchema = {
    value: ['email'],
  };

  const result = validateData(data, validationSchema);

  expect(result).toBe(true);
});

// Individual rules
test('return an error if a "notEmpty" parameter is empty', () => {
  const data = {
    value: '',
  };

  const validationSchema = {
    value: ['notEmpty'],
  };

  const result = validateData(data, validationSchema);

  expect(result.code).toBe(1001);
  expect(result.errors.value).toBe('notEmpty');
});

test('return an error if an "email" parameter is incorrect', () => {
  const data = {
    value: 'wrong',
  };

  const validationSchema = {
    value: ['email'],
  };

  const result = validateData(data, validationSchema);

  expect(result.code).toBe(1001);
  expect(result.errors.value).toBe('email');
});

test('return an error if an "emails" parameter is incorrect', () => {
  const data = {
    value: ['wrong', 'correct@email.com'],
  };

  const validationSchema = {
    value: ['emails'],
  };

  const result = validateData(data, validationSchema);

  expect(result.code).toBe(1001);
  expect(result.errors.value).toBe('emails');
});

test('return an error if an "isoDate" parameter is incorrect', () => {
  const data = {
    value: new Date(),
  };

  const validationSchema = {
    value: ['isoDate'],
  };

  const result = validateData(data, validationSchema);

  expect(result.code).toBe(1001);
  expect(result.errors.value).toBe('isoDate');
});

test('return an error if an "alphaNum" parameter is incorrect', () => {
  const data = {
    value: 'dwd@!#453',
  };

  const validationSchema = {
    value: ['alphaNum'],
  };

  const result = validateData(data, validationSchema);

  expect(result.code).toBe(1001);
  expect(result.errors.value).toBe('alphaNum');
});

test('return an error if an "alphaNumDash" parameter is incorrect', () => {
  const data = {
    value: 'dwd@!#453',
  };

  const validationSchema = {
    value: ['alphaNumDash'],
  };

  const result = validateData(data, validationSchema);

  expect(result.code).toBe(1001);
  expect(result.errors.value).toBe('alphaNumDash');
});

test('return an error if an "alphaNumDash" parameter begins with a dash character', () => {
  const data = {
    value: '-dwdDWqd59',
  };

  const validationSchema = {
    value: ['alphaNumDash'],
  };

  const result = validateData(data, validationSchema);

  expect(result.code).toBe(1001);
  expect(result.errors.value).toBe('alphaNumDash');
});

test('return an error if an "alphaNumDash" parameter ends with a dash character', () => {
  const data = {
    value: 'dwdDWqd59-',
  };

  const validationSchema = {
    value: ['alphaNumDash'],
  };

  const result = validateData(data, validationSchema);

  expect(result.code).toBe(1001);
  expect(result.errors.value).toBe('alphaNumDash');
});

test('return an error if an "phoneNumber" is not valid', () => {
  const data = {
    value: '+6140417a4106' // the good one for nudes is +61404174106
  };

  const validationSchema = {
    value: ['phoneNumber']
  };

  const result = validateData(data, validationSchema);

  expect(result.code).toBe(1001);
  expect(result.errors.value).toBe('phoneNumber');
});

test('return an error if an "alphaNumDashNoCap" parameter is incorrect', () => {
  const data = {
    value: 'djwqd497-878Y',
  };

  const validationSchema = {
    value: ['alphaNumDashNoCap'],
  };

  const result = validateData(data, validationSchema);

  expect(result.code).toBe(1001);
  expect(result.errors.value).toBe('alphaNumDashNoCap');
});

test('return an error if an "ids" parameter is incorrect', () => {
  const data = {
    value: ['dowqid', 'djwqd497-878Y'],
  };

  const validationSchema = {
    value: ['ids'],
  };

  const result = validateData(data, validationSchema);

  expect(result.code).toBe(1001);
  expect(result.errors.value).toBe('ids');
});

test('return a list of errors if multiple parameters are incorrect', () => {
  const data = {
    name: '123D Dwdwq',
    email: 'wrong',
  };

  const validationSchema = {
    name: ['alphaNumDashNoCap'],
    email: ['email'],
    role: ['required'],
  };

  const result = validateData(data, validationSchema);

  expect(result).toEqual({
    'code': 1001,
    'errors': {
      'email': 'email',
      'name': 'alphaNumDashNoCap',
      'role': 'required'
    },
    'message': 'Validation errors',
  });
});
