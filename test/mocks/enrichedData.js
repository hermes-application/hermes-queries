const mockedData = {
  invitation: {
    id: 1,
    organizerId: 1,
    roomId: 1,
    guestsAcceptedIds: [1, 1],
    guestsPendingIds: [1, 1],
    guestsRefusedIds: [1, 1],
  },
  meeting: {
    id: 1,
    organizerId: 1,
    roomId: 1,
    guestsAcceptedIds: [1, 1],
    guestsPendingIds: [1, 1],
    guestsRefusedIds: [1, 1],
  },
  user: { currentRoomId: 1 },
  room: { beaconsIds: [1, 1] },
  beacon: { id: 1 },
  teamInvitation: { id: 1 },
};

const enrichedMockedData = {
  invitation: {
    "id": 1,
    "organizerId": 1,
    "roomId": 1,
    "guestsAcceptedIds": [1, 1],
    "guestsPendingIds": [1, 1],
    "guestsRefusedIds": [1, 1],
    "organizer": {
      "currentRoomId": 1,
      "currentRoom": {
        "beaconsIds": [1, 1],
        "beacons": [{
          "id": 1
        }, {
          "id": 1
        }]
      }
    },
    "room": {
      "beaconsIds": [1, 1],
      "beacons": [{
        "id": 1
      }, {
        "id": 1
      }]
    },
    "guestsAccepted": [{
      "currentRoomId": 1,
      "currentRoom": {
        "beaconsIds": [1, 1],
        "beacons": [{
          "id": 1
        }, {
          "id": 1
        }]
      }
    }, {
      "currentRoomId": 1,
      "currentRoom": {
        "beaconsIds": [1, 1],
        "beacons": [{
          "id": 1
        }, {
          "id": 1
        }]
      }
    }],
    "guestsPending": [{
      "currentRoomId": 1,
      "currentRoom": {
        "beaconsIds": [1, 1],
        "beacons": [{
          "id": 1
        }, {
          "id": 1
        }]
      }
    }, {
      "currentRoomId": 1,
      "currentRoom": {
        "beaconsIds": [1, 1],
        "beacons": [{
          "id": 1
        }, {
          "id": 1
        }]
      }
    }],
    "guestsRefused": [{
      "currentRoomId": 1,
      "currentRoom": {
        "beaconsIds": [1, 1],
        "beacons": [{
          "id": 1
        }, {
          "id": 1
        }]
      }
    }, {
      "currentRoomId": 1,
      "currentRoom": {
        "beaconsIds": [1, 1],
        "beacons": [{
          "id": 1
        }, {
          "id": 1
        }]
      }
    }]
  },
  meeting: {
    "id": 1,
    "organizerId": 1,
    "roomId": 1,
    "guestsAcceptedIds": [1, 1],
    "guestsPendingIds": [1, 1],
    "guestsRefusedIds": [1, 1],
    "organizer": {
      "currentRoomId": 1,
      "currentRoom": {
        "beaconsIds": [1, 1],
        "beacons": [{
          "id": 1
        }, {
          "id": 1
        }]
      }
    },
    "room": {
      "beaconsIds": [1, 1],
      "beacons": [{
        "id": 1
      }, {
        "id": 1
      }]
    },
    "guestsAccepted": [{
      "currentRoomId": 1,
      "currentRoom": {
        "beaconsIds": [1, 1],
        "beacons": [{
          "id": 1
        }, {
          "id": 1
        }]
      }
    }, {
      "currentRoomId": 1,
      "currentRoom": {
        "beaconsIds": [1, 1],
        "beacons": [{
          "id": 1
        }, {
          "id": 1
        }]
      }
    }],
    "guestsPending": [{
      "currentRoomId": 1,
      "currentRoom": {
        "beaconsIds": [1, 1],
        "beacons": [{
          "id": 1
        }, {
          "id": 1
        }]
      }
    }, {
      "currentRoomId": 1,
      "currentRoom": {
        "beaconsIds": [1, 1],
        "beacons": [{
          "id": 1
        }, {
          "id": 1
        }]
      }
    }],
    "guestsRefused": [{
      "currentRoomId": 1,
      "currentRoom": {
        "beaconsIds": [1, 1],
        "beacons": [{
          "id": 1
        }, {
          "id": 1
        }]
      }
    }, {
      "currentRoomId": 1,
      "currentRoom": {
        "beaconsIds": [1, 1],
        "beacons": [{
          "id": 1
        }, {
          "id": 1
        }]
      }
    }]
  },
  user: {
    "currentRoomId": 1,
    "currentRoom": {
      "beaconsIds": [1, 1],
      "beacons": [{
        "id": 1
      }, {
        "id": 1
      }]
    }
  },
  room: {
    "beaconsIds": [1, 1],
    "beacons": [{
      "id": 1
    }, {
      "id": 1
    }]
  },
  beacon: {
    "id": 1
  },
  teamInvitation: {
    "id": 1
  }
};

module.exports = {
  mockedData,
  enrichedMockedData,
};
