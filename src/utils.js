"use strict";

function mapObject(object, callback) {
  return Object.keys(object || {}).map(key => callback(object[key], key));
}

module.exports = {
  mapObject,
};
