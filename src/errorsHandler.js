// Errors:
const messages = {
  1000: "No valid validation schema provided",
  1001: "Validation errors",
  1002: "Insufficient user permissions",
  1003: "Data does not exist in DB",
};

module.exports = (code, errors = null) => {
  return {
    code,
    message: messages[code],
    errors,
  };
}
