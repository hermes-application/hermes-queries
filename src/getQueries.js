"use strict";

const teams = (Parse, currentUser) => {
  let query = Parse.Object.extend('Team');
  query = new Parse.Query(query);
  query.equalTo('objectId', currentUser.get('teamId'));
  query.notEqualTo('status', 'deleted');
  return query;
};

const teamInvitations = (Parse, currentUser) => {
  let query = Parse.Object.extend('TeamInvitation');
  query = new Parse.Query(query);
  query.equalTo('teamId', currentUser.get('teamId'));
  query.notEqualTo('status', 'deleted');

  return query;
};

const invitations = (Parse, currentUser) => {
  let query = Parse.Object.extend('Meeting');
  query = new Parse.Query(query);
  query.containedIn('guestsPendingIds', [currentUser.id]);
  query.equalTo('teamId', currentUser.get('teamId'));
  query.notEqualTo('status', 'deleted');

  return query;
};

const meetings = (Parse, user, otherOptions = {}) => {
  const { fromDate, toDate } = otherOptions;

  // Get all meetings the user organized
  let organizerQuery = Parse.Object.extend('Meeting');
  organizerQuery = new Parse.Query(organizerQuery);
  organizerQuery.equalTo('organizerId', user.id);
  organizerQuery.equalTo('teamId', user.get('teamId'));
  organizerQuery.notEqualTo('status', 'deleted');

  // Get all meetings the user is participating to (excluding the ones he organized)
  let userMeetingsQuery = Parse.Object.extend('Meeting');
  userMeetingsQuery = new Parse.Query(userMeetingsQuery);
  userMeetingsQuery.equalTo('teamId', user.get('teamId'));
  userMeetingsQuery.containedIn('guestsAcceptedIds', [user.id]);
  userMeetingsQuery.notEqualTo('status', 'deleted');

  let query = new Parse.Query.or(organizerQuery, userMeetingsQuery);

  if (fromDate) {
    query.greaterThanOrEqualTo('startTime', fromDate);
  }

  if (toDate) {
    query.lessThanOrEqualTo('endTime', toDate);
  }

  return query;
};

const _createFakeUserObject = (id, teamId) => ({
  id,
  teamId,
  get: key => this[key],
});

// Query to get meetings the current user has neither been invited to nor organized
// Note: only non-sensitive fields will be retrieved (used to get rooms calendars and users calendars)
const otherMeetings = (Parse, currentUser, otherOptions = {}) => {
  const { roomId, userId, fromDate, toDate } = otherOptions;

  let query = Parse.Object.extend('Meeting');
  query = new Parse.Query(query);

  if (roomId) {
    query.equalTo('roomId', roomId);
  }

  if (userId) {
    // Get meetings of another user
    // Note: we don't retrieve the invitations as well, as a pending invitation means the
    // user hasn't accepted to participate yet, so he's still free during those time slots
    const fakeUser = _createFakeUserObject(userId, currentUser.get('teamId'));
    const userMeetingsQuery = meetings(Parse, fakeUser);

    query = new Parse.Query.or(query, userMeetingsQuery);
  }

  if (fromDate) {
    query.greaterThanOrEqualTo('startTime', fromDate);
  }

  if (toDate) {
    query.lessThanOrEqualTo('endTime', toDate);
  }

   // Exclude current user's meetings and invitations
  query.notEqualTo('organizerId', currentUser.id);
  query.notContainedIn('guestsAcceptedIds', [currentUser.id]);
  query.notContainedIn('guestsPendingIds', [currentUser.id]);

  query.equalTo('teamId', currentUser.get('teamId'));
  query.notEqualTo('status', 'deleted');

  // Return non-sensitive fields only
  query.select('startTime', 'endTime', 'roomId', 'teamId', 'status');

  return query;
};

const rooms = (Parse, currentUser) => {
  let query = Parse.Object.extend('Room');
  query = new Parse.Query(query);
  query.equalTo('teamId', currentUser.get('teamId'));
  query.notEqualTo('status', 'deleted');

  return query;
};

const beacons = (Parse, currentUser) => {
  let query = Parse.Object.extend('Beacon');
  query = new Parse.Query(query);
  query.equalTo('teamId', currentUser.get('teamId'));
  query.notEqualTo('status', 'deleted');

  return query;
};

const users = (Parse, currentUser) => {
  let query = Parse.Object.extend('_User');
  query = new Parse.Query(query);
  query.equalTo('teamId', currentUser.get('teamId'));
  query.notEqualTo('status', 'deleted');

  return query;
};

const sessions = (Parse) => {
  let query = Parse.Object.extend('_Session');
  query = new Parse.Query(query);

  return query;
}

module.exports = {
  teams,
  teamInvitations,
  invitations,
  meetings,
  otherMeetings,
  users,
  rooms,
  beacons,
  sessions,
};
