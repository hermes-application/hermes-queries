"use strict";

const objectAssign = require('object-assign');
const moment = require('moment');
const formatObject = require('./formater');
const { mapObject } = require('./utils');

function enrichMeetings(meetings, users, rooms, beacons) {
  // Check we have enough data
  if (!meetings || !users || !rooms || !beacons) {
    return [];
  }

  return mapObject(meetings, (meeting, id) => enrichMeeting(meeting, users, rooms, beacons));
};

function enrichMeeting(meeting, users, rooms, beacons) {
  // Check we have enough data
  if (!meeting || !users || !rooms || !beacons) {
    return null;
  }

  const enrichedMeeting = objectAssign({}, meeting);

  // Get organizer
  enrichedMeeting.organizer = users.hasOwnProperty(enrichedMeeting.organizerId) ? enrichUser(users[enrichedMeeting.organizerId], rooms, beacons) : null;
  // delete enrichedMeeting.organizerId;

  // Get room
  enrichedMeeting.room = rooms.hasOwnProperty(enrichedMeeting.roomId) ? enrichRoom(rooms[enrichedMeeting.roomId], beacons) : null;
  // delete enrichedMeeting.roomId;

  // Get guests
  enrichedMeeting.guestsAccepted = enrichedMeeting.guestsAcceptedIds.map(guestAcceptedId => users.hasOwnProperty(guestAcceptedId) ? enrichUser(users[guestAcceptedId], rooms, beacons) : null);
  // delete enrichedMeeting.guestsAcceptedIds;

  enrichedMeeting.guestsPending = enrichedMeeting.guestsPendingIds.map(guestPendingId => users.hasOwnProperty(guestPendingId) ? enrichUser(users[guestPendingId], rooms, beacons) : null);
  // delete enrichedMeeting.guestsPendingIds;

  enrichedMeeting.guestsRefused = enrichedMeeting.guestsRefusedIds.map(guestRefusedId => users.hasOwnProperty(guestRefusedId) ? enrichUser(users[guestRefusedId], rooms, beacons) : null);
  // delete enrichedMeeting.guestsRefusedIds;

  return enrichedMeeting;
}

// --------------------------------------------------------------------------------------------------------------------------------------------

function enrichUsers(users, rooms, beacons) {
  // Check we have enough data
  if (!users || !rooms) {
    return [];
  }

  return mapObject(users, (user, id) => enrichUser(user, rooms, beacons));
};

function enrichUser(user, rooms, beacons) {
  // Check we have enough data
  if (!user || !rooms || !beacons) {
    return null;
  }

  const enrichedUser = objectAssign({}, user);

  // Get room
  enrichedUser.currentRoom = rooms.hasOwnProperty(enrichedUser.currentRoomId) ? enrichRoom(rooms[enrichedUser.currentRoomId], beacons) : null;
  // delete enrichedUser.currentRoomId;

  return enrichedUser;
}

// --------------------------------------------------------------------------------------------------------------------------------------------

function enrichRooms(rooms, beacons) {
  // Check we have enough data
  if (!rooms || !beacons) {
    return [];
  }

  return mapObject(rooms, (room, id) => enrichRoom(room, beacons));
}

function enrichRoom(room, beacons) {
  // Check we have enough data
  if (!room || !beacons) {
    return null;
  }

  const enrichedRoom = objectAssign({}, room);

  // Get beacons
  enrichedRoom.beacons = enrichedRoom.beaconsIds.map(beaconsId => beacons.hasOwnProperty(beaconsId) ? enrichBeacon(beacons[beaconsId]) : null);

  return enrichedRoom;
}

// --------------------------------------------------------------------------------------------------------------------------------------------

function enrichBeacons(beacons) {
  // Check we have enough data
  if (!beacons) {
    return [];
  }

  return mapObject(beacons, (beacon, id) => enrichBeacon(beacon));
}

function enrichBeacon(beacon) {
  // Check we have enough data
  if (!beacon) {
    return null;
  }

  // No processing yet
  return beacon;
}

// --------------------------------------------------------------------------------------------------------------------------------------------

function enrichTeamInvitations(invitations) {
  // Check we have enough data
  if (!invitations) {
    return [];
  }

  return mapObject(invitations, (invitation, id) => enrichTeamInvitation(invitation));
};

function enrichTeamInvitation(invitation) {
  // Check we have enough data
  if (!invitation) {
    return null;
  }

  // No processing yet
  return invitation;
}

// --------------------------------------------------------------------------------------------------------------------------------------------

module.exports = {
  mapObject,
  enrichInvitations: enrichMeetings,
  enrichInvitation: enrichMeeting,
  enrichMeetings,
  enrichMeeting,
  enrichUsers,
  enrichUser,
  enrichRooms,
  enrichRoom,
  enrichBeacons,
  enrichBeacon,
  enrichTeamInvitations,
  enrichTeamInvitation
};
