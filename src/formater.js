"use strict";

// Note: the validator is only used to check the data sent TO the server BY the client
module.exports = (data, type) => {
  if (!data) {
    return null;
  }

  if (type === 'teams') {
    return {
      id: data.id,
      type,
      customId: data.get('customId'),
      name: data.get('name'),
      billingId: data.get('billingId') || null,
      addressStreet: data.get('addressStreet') || null,
      addressSuburb: data.get('addressSuburb') || null,
      addressState: data.get('addressState') || null,
      email: data.get('email'),
      status: data.get('status'),
    }
  }

  if (type === 'teamInvitations') {
    return {
      id: data.id,
      type,
      email: data.get('email'),
      firstName: data.get('firstName'),
      lastName: data.get('lastName'),
      sentDate: data.get('updatedAt').toISOString(),
      status: data.get('status'),
    }
  }

  if (type === 'meetings' || type === 'otherMeetings' || type === 'invitations') {
    return {
      id: data.id,
      type,
      name: data.get('name') || null,
      startTime: data.get('startTime').toISOString(),
      endTime: data.get('endTime').toISOString(),
      organizerId: data.get('organizerId') || null,
      roomId: data.get('roomId'),
      guestsPendingIds: data.get('guestsPendingIds') || [],
      guestsAcceptedIds: data.get('guestsAcceptedIds') || [],
      guestsRefusedIds: data.get('guestsRefusedIds') || [],
      status: data.get('status'),
    }
  }

  if (type === 'users') {
    return {
      id: data.id,
      type,
      username: data.get('searchUsername'),
      email: data.get('userEmail'),
      firstName: data.get('firstName'),
      lastName: data.get('lastName'),
      jobPosition: data.get('jobPosition') || null,
      phoneNumber: data.get('phoneNumber') || null,
      avatarUrl: (data.get('avatar') && data.get('avatar').url()) || null,
      currentRoomId: data.get('currentRoomId') || null,
      joinDate: data.get('createdAt').toISOString(),
      teamId: data.get('teamId'),
      role: data.get('role'),
      status: data.get('status'),
    }
  }

  if (type === 'rooms') {
    return {
      id: data.id,
      type,
      name: data.get('name'),
      notes: data.get('notes') || null,
      color: data.get('color'),
      beaconsIds: data.get('beaconsIds') || [],
      status: data.get('status'),
    }
  }

  if (type === 'beacons') {
    return {
      id: data.id,
      type,
      uuid: data.get('uuid'),
      range: data.get('range'),
      status: data.get('status'),
    }
  }

  // Wrong type provided, return error
  throw new Error(`Wrong item type provided: ${type}.`);
};
