const trim = require('trim');
const emailValidator = require('email-validation');
const phoneValidator = require('phone-regex');
const moment = require('moment');

const generateError = require('./errorsHandler');
const { mapObject } = require('./utils');

// Notes: modifies original errorsArray reference
function validateParam(errors, param, newError) {
  if (!errors[param] || !errors[param].length) {
    errors[param] = [];
  }

  errors[param].push(newError);
}

// Note: the validator is only used to check the data sent TO the server BY the client
module.exports = (data, validationSchema) => {
  if (!validationSchema) {
    return generateError(1000);
  }

  const errors = {};

  let result = true;

  mapObject(validationSchema, (rules, param) => {
    const value = data[param];

    // Check input has been trimmed
    if (typeof(value) === 'string' && rules.indexOf('password') < 0 && trim(value) !== value) {
      errors[param] = 'trim';
    }

    // Check individual rules (if input has been trimmed)
    if (!errors[param]) {
      rules.map((rule) => {
        if (rule === 'required' && typeof value === 'undefined') {
          errors[param] = rule;
          return
        }

        // Input has not been provided, but is optional so it passes validation
        if (typeof value === 'undefined') {
          return
        }

        if (rule === 'notEmpty' && value.length === 0) {
          errors[param] = rule;
          return
        }

        if (rule === 'email' && !emailValidator.valid(value)) {
          errors[param] = rule;
          return
        }

        // check an array of emails
        if (rule === 'emails') {
          for (let email of value) {
            if (!emailValidator.valid(email)) {
              errors[param] = rule;
              return
            }
          }
        }

        if (rule === 'isoDate' && (typeof value !== 'string' || !moment(value, moment.ISO_8601).isValid())) {
          errors[param] = rule;
          return
        }

        const regexAlphaNum = /^[a-zA-Z0-9]+$/;

        if (rule === 'alphaNum' && !regexAlphaNum.test(value)) {
          errors[param] = rule;
          return
        }

        const regexAlphaNumDash = /^[a-zA-Z0-9-]+$/;

        if (rule === 'alphaNumDash' && (value[0] === '-' || value[value.length-1] === '-' || !regexAlphaNumDash.test(value))) {
          errors[param] = rule;
          return
        }

        const regexAlphaNumDashNoCap = /^[a-z0-9-]+$/;

        if (rule === 'alphaNumDashNoCap' && (value[0] === '-' || value[value.length-1] === '-' || !regexAlphaNumDashNoCap.test(value))) {
          errors[param] = rule;
          return
        }

        if (rule === 'phoneNumber' && !phoneValidator({ exact: true }).test(value)) {
          if(value === null) {
            return;
          }

          errors[param] = rule;
          return
        }

        // Check an array of IDs
        if (rule === 'ids') {
          for (let id of value) {
            if (!regexAlphaNum.test(id)) {
              errors[param] = rule;
              return
            }
          }
        }
      });
    }

    if (Object.keys(errors).length > 0) {
      result = generateError(1001, errors);
    }
  });

  return result;
};